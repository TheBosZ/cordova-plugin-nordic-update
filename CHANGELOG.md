# Changelog

## 0.1.0

-   Modify iOS errors to more closely match Android

## 0.0.1

-   Initial release
